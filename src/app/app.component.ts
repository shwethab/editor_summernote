import { Component } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import {HttpClient,HttpHeaders} from '@angular/common/http'
import { sanitizeHtml } from '@angular/core/src/sanitization/sanitization';

declare var $;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {  
  form: FormGroup;
  content:any;
  key='';
  config: any = {
    height: '200px',
    uploadImagePath: '',
    toolbar: [
      ['misc', ['codeview', 'undo', 'redo', 'codeBlock']],
      ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
      ['fontsize', ['fontname', 'fontsize', 'color']],
      ['para', ['style0', 'ul', 'ol', 'paragraph', 'height']],
      ['insert', ['table', 'picture', 'link', 'video', 'hr']],
      ['customButtons', ['testBtn']]
    ],
    buttons: {
      'testBtn': this.customButton()
    }
  };
  
  editorDisabled = false;


  constructor(
    private sanitizer: DomSanitizer,private http:HttpClient
  ) {
    this.form = new FormGroup({
      html: new FormControl()
    });
  }

  enableEditor() {
    this.editorDisabled = false;
  }
  get sanitizedHtml() {
    return this.sanitizer.bypassSecurityTrustHtml(this.form.get('html').value);
  }

  disableEditor() {
    this.editorDisabled = true;
  }

  onBlur() {
    console.log('Blur');
  }
  savedata(): void{
    let p=this.form.get('html').value;
    let d=this.randomString();
    this.key=d;
    this.postdata(d,p).then((res)=>{
      console.log(res)
    })
  }
  retData(){
    let a="eXx7TIfJ";
      return new Promise((resolve,reject)=>{
        this.getdata(this.key).subscribe((res)=>{
                this.content=res;
                console.log(res)
              resolve(true)
         })
      }).catch(()=>console.log('nothing came'))
     }
  getdata(a){
    return this.http.get("http://10.60.68.74:8087/get/"+a)
  }
   postdata(d,p){
     let ob={"key":d,"data":p}
     console.log(ob);
     const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json'})
    };
    return this.http.post("http://10.60.68.74:8087/store",ob,httpOptions).toPromise()
   }
  randomString() {
    var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";
    var string_length = 8;
    var randomstring = '';
    for (var i=0; i<string_length; i++) {
      var rnum = Math.floor(Math.random() * chars.length);
      randomstring += chars.substring(rnum,rnum+1);
    }
    return randomstring;
  }
  customButton() {
    return (context) => {
      const ui = $.summernote.ui;
      const button = ui.button({
        contents: 'Test btn',
        tooltip: 'Test',
        click: function () {
          context.invoke('editor.pasteHTML', '<div>Hello from test btn!!!!</div>');
        }
      });
      return button.render();
    }
  }
}
